import React from "react";

const ValidarCita = () => (
    <div className="alert alert-danger mt-2 mb-5 text-center">Todos los campos son obligatorios</div>
);

export default ValidarCita;