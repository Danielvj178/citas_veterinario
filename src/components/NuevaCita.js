import React, { Component } from "react";
import ValidarCita from "./ValidarCita";
import uuid from 'uuid';
import PropTypes from 'prop-types';

const stateInicial = {
    cita: {
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
    },
    error: false
}

class NuevaCita extends Component {
    state = {
        ...stateInicial
    }

    handleChange = e => {
        this.setState({
            cita: {
                // Se copia el state para no sobreescribirlos
                ...this.state.cita,
                [e.target.name]: e.target.value
            }
        })
    }

    handleSubmit = e => {
        e.preventDefault();

        // Extracción de valores
        const { mascota, propietario, fecha, hora, sintomas } = this.state.cita;

        //Validar los campos digitados
        if (mascota == '' || propietario == '' || fecha == '' || sintomas == '') {
            this.setState({
                error: true
            });

            return;
        }

        //Agregar cita
        const nuevaCita = { ...this.state.cita };
        nuevaCita.id = uuid();
        this.props.crearNuevaCita(nuevaCita);

        // Colocar el state en el StateInicial
        this.setState({
            ...stateInicial
        })
    }

    render() {

        //Extraer valor del error
        const { error } = this.state;

        return (
            <div className="card mt-5 py-5">
                <div className="card-body">
                    <h2 className="card-tittle text-center mb-5">
                        Llena el formulario para crear una nueva cita
                    </h2>

                    {error ? <ValidarCita /> : null}

                    <form
                        onSubmit={this.handleSubmit}
                    >
                        {/* Nombre mascota */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-lable">Nombre</label>
                            <div className="col-sm-8 col-lg-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre mascota"
                                    name="mascota"
                                    onChange={this.handleChange}
                                    value={this.state.cita.mascota}
                                />
                            </div>
                        </div>
                        {/* Nombre dueño */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-lable">Nombre dueño</label>
                            <div className="col-sm-8 col-lg-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre dueño mascota"
                                    name="propietario"
                                    onChange={this.handleChange}
                                    value={this.state.cita.propietario}
                                />
                            </div>
                        </div>
                        {/* Fecha de cita y Hora cita*/}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-lable">Fecha cita</label>
                            <div className="col-sm-8 col-lg-4">
                                <input
                                    type="date"
                                    className="form-control"
                                    name="fecha"
                                    onChange={this.handleChange}
                                    value={this.state.cita.fecha}
                                />
                            </div>

                            <label className="col-sm-4 col-lg-2 col-form-lable">Hora cita</label>
                            <div className="col-sm-8 col-lg-4">
                                <input
                                    type="time"
                                    className="form-control"
                                    name="hora"
                                    onChange={this.handleChange}
                                    value={this.state.cita.hora}
                                />
                            </div>
                        </div>
                        {/* Motivo visita */}
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-lable">Síntomas</label>
                            <textarea
                                className="form-control"
                                name="sintomas"
                                placeholder="Motivo de visita"
                                onChange={this.handleChange}
                                value={this.state.cita.sintomas}
                            ></textarea>
                        </div>

                        <input
                            type="submit"
                            className="py-3 mt-2 btn btn-success btn-block"
                            value="Agregar nueva cita"
                        />
                    </form>
                </div>
            </div>
        );
    }
}

NuevaCita.propTypes = {
    crearNuevaCita: PropTypes.func.isRequired
}

export default NuevaCita;