import React, { Component } from "react";
import "./bootstrap.min.css";
import Header from "./components/Header";
import NuevaCita from "./components/NuevaCita";
import ListaCitas from "./components/ListaCitas";

class App extends Component {
  state = {
    citas: []
  }

  crearNuevaCita = datos => {
    //Copiar el state actual
    const citas = [...this.state.citas, datos];

    this.setState({
      citas
    });
  }

  // Cuando la aplicación carga
  componentDidMount() {
    const citasLS = localStorage.getItem('citas');
    if (citasLS) {
      this.setState({
        citas: JSON.parse(citasLS)
      })
    }
  }

  // Cuando eliminamos o agregamos una nueva cita
  componentDidUpdate() {
    localStorage.setItem('citas', JSON.stringify(this.state.citas));
  }

  // Elimnar citas
  eliminarCita = id => {
    //Tomar copia del state
    const citasActuales = [...this.state.citas];

    // Utilizar filter para sacar el elemento a eliminar
    // Colocamnos diferente porque vamos a obtener el arreglo sin el elemento que vamos a eliminar
    const citas = citasActuales.filter(cita => cita.id !== id)

    // Actualizamos el state
    this.setState({
      citas
    });
  }

  render() {
    return (
      <div className="container">
        <Header
          titulo='Administrador Pacientes Veterinaria'
        />

        <div className="row">
          <div className="col-md-10 mx-auto">
            <NuevaCita
              crearNuevaCita={this.crearNuevaCita}
            />
          </div>
        </div>

        <div className="mt-5 col-md-10 mx-auto">
          <ListaCitas
            citas={this.state.citas}
            eliminarCita={this.eliminarCita}
          />
        </div>
      </div>
    );
  }
}

export default App;
